import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { BlogRootRoutingModule } from './blog-root-routing.module'

@NgModule({
  declarations: [],
  imports: [CommonModule, BlogRootRoutingModule],
})
export class BlogRootModule {}

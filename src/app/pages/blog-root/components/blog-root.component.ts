import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { ActivatedRoute, Router, ROUTES } from '@angular/router'

declare var ng: any

@Component({
  selector: 'app-blog-root',
  templateUrl: './blog-root.component.html',
  styleUrls: ['./blog-root.component.scss'],
  preserveWhitespaces: true,
  encapsulation: ViewEncapsulation.Emulated,
})
export class BlogRootComponent implements OnInit {
  ngOnInit() {}

  constructor(private router: Router, private route: ActivatedRoute) {}
}
